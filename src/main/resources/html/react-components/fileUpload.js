var FileUpload = React.createClass({
    getDefaultProps: function() {
        return {
            path:'',
            canSubmit:false,
            onSubmit:function(){}
        }
    },
    getInitialState: function() {
        var that=this
        var count=0
        function bindJava(){
            if(window.JavaApp){
                that.setState({JavaApp:true})
                console.log("java app bound in "+count*100+"ms! will choose file from javaFX without upload")
            }else{
                if(count==0){
                    console.log("window.JavaApp not bind!")                    
                }
                count++
                setTimeout(bindJava,100)
            }
        }

        bindJava()

        return {
            title: '文件名-未上传',
            upload: "init",
            path:this.props.path,
            JavaApp:false
        }
    },
    selectFromJavaFX:function(evt){
        var path = window.JavaApp.selectFile()
        console.log(path)
        if(path){
            this.setState({
            	path: path,
            	title: getFileNameFromPath(path),
            	upload: 'finish'
            })
            this.props.onUploadDone(path)
        }
    },
    fileSelected: function(event){
        var file = document.getElementById('xxx').files[0];
        if (file) {
            this.setState({upload:'uploading', title:file.name, path:file.name})
            this.uploadFile(file)
        }
    },
    uploadFile: function(file) {
        var fd = new FormData();
        fd.append("fileToUpload", file)
        fd.append('fileName', file.name)
        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", this.progress, false);
        xhr.addEventListener("load", this.done, false);
        xhr.addEventListener("error", this.error, false);
        xhr.addEventListener("abort", this.cancel, false);
        xhr.open("POST", "/post/import/upload");
        xhr.send(fd);
    },
    progress: function(evt){
        if (evt.lengthComputable) {
            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
            this.setState({
                button:'上传中' + percentComplete.toString() + '%',
                progress: percentComplete,
                upload: 'uploading'
            })
        }else{
            this.error()
        }
    },
    done: function(evt){
        this.setState({
            upload:'done',
            button:'完成',
            progress: 100
        })
        var that = this
        setTimeout(function(){
            that.setState({upload:'finish'})
        },4000)
        this.props.onUploadDone(this.state.path)
    },
    error: function(){
        this.setState({button:'出错了',upload:'failed'})
    },
    cancel: function(){
        this.setState({button:'取消了',upload:'failed'})
    },
    submit:function(){
    	if(this.props.canSubmit){
    		this.props.onSubmit()
    	}
    },
    render: function() {
        var busy
        switch(this.state.upload){
            case 'uploading': busy = 'ui right floated button loading'; break;
            default:
                if(this.props.canSubmit){
                    busy = 'ui right floated button positive'
                }else{
                    busy = 'ui right floated button disabled'
                }
        }

        return (
            <div className="ui sticky">
                <h2 className="ui top attached header">
                    <span id="title">{this.state.title}</span>
                    <button className={busy} onClick={this.submit}>提交</button>
                    <label htmlFor={this.state.JavaApp?'':'xxx'} className="ui right floated primary button" style={this.state.upload=='init'?{}:{display: 'none'}} onClick={this.state.JavaApp?this.selectFromJavaFX:''}>
                        上传文件
                        <i className="right chevron icon"></i>
                        <input id="xxx" type="file" style={{display: 'none'}} onChange={this.state.JavaApp?'':this.fileSelected} />
                    </label>
                    <button className="ui right floated button" style={
                        (this.state.upload=='uploading' || this.state.upload=='done')?{}:{display: 'none'}
                    }>
                        <i className="checkmark icon" style={this.state.upload=='done'?{}:{display: 'none'}}></i>
                        {this.state.button}
                    </button>
                    <div className="sub header">
                        <i className="folder icon"></i>
                        <span>{this.props.path.length?getFilePath(this.props.path):'路径（空）'}</span>
                    </div>
                </h2>

                <div className="ui active bottom attached indicating progress"
                    style={this.state.upload=='uploading'?{}:{display: 'none'}}>
                    <div className="bar"></div>
                </div>
            </div>
        );
    }
});

