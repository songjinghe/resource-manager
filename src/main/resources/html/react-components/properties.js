var InputPanel = React.createClass({
	getDefaultProps: function() {
        return {
            edit:{
            	modal:false,
            	key:'',
            	value:''
            }
        };
    },
    newUserPropertyFromEnter: function(event){
        if(event.keyCode == 13){
            this.newProperty()
        }
    },

    newUserPropertyFromButton: function(event){
    	this.newProperty()
    },

	biggerInputArea:function(){
		var that = this
		$('#add-property-modal').modal({
			onApprove : function(){
				that.newProperty('fromModal')
			}
		}).modal('show');
	},

	clearModalContent:function(){
    	this.refs.key0.value = ''
    	this.refs.value0.value=''
    },

    newProperty:function(fromModal, old){
    	var key,value;
		if(fromModal){
			key = this.refs.key0.value.trim()
			value = this.refs.value0.value.trim()
		}else{
			key = this.refs.key1.value.trim()
			value = this.refs.value1.value.trim()
		}
		// console.log(key,value)
    	this.props.callback(key, value, fromModal, old);
    },

    componentDidMount:function(){
   		// console.log('first mount')
    },

    componentDidUpdate:function(prevProps, prevState){
    	if(this.props.edit.modal){
	   		// console.log('really modal!!!!~~~')
	   		var preEdit = this.props.edit
    		var that = this
			$('#add-property-modal').modal({
				onApprove : function(){
					that.newProperty('fromModal',preEdit)
				}
			}).modal('show');
    	}
    },

    render: function(){
    	// console.log('render input',this.props)
    	return (
    		<div className="ui">
                <div className="ui input">
                    <input ref="key1" type="text" name="key" placeholder="属性名称" defaultValue={this.props.edit.modal?'':this.props.edit.key} />
                </div>
                <div className="ui action input">
                    <input ref="value1" type="text" name="value" placeholder="属性值" onKeyUp={this.newUserPropertyFromEnter} defaultValue={this.props.edit.modal?this.props.edit.key:''} />
                    <button className="ui button" onClick={this.biggerInputArea}>更大输入框</button>
                </div>
                <div className="ui input">
                    <button className="ui button primary" onClick={this.newUserPropertyFromButton}>添加</button>
                </div>

                <div id="add-property-modal" className="ui modal">
					<i className="close icon"></i>
					<div className="header">
						请输入属性
					</div>
					<div className="content">
						<div className="ui form">
							<div className="field">
								<div className="ui fluid icon input">
									<input ref="key0" type="text" placeholder="属性名称" defaultValue={this.props.edit.modal?this.props.edit.key:''} />
									<i className="search icon"></i>
								</div>
							</div>
							<div className="field">
								<label>属性值</label>
								<textarea ref="value0" defaultValue={this.props.edit.modal?this.props.edit.key:''}></textarea>
							</div>
						</div>
					</div>
					<div className="actions">
						<div className="ui negative button" onClick={this.clearModalContent}>清空</div>
						<div className="ui cancel button">取消</div>
						<div className="ui positive approve right labeled icon button">
							添加
							<i className="checkmark icon"></i>
						</div>
					</div>
				</div>
            </div>
    	)
    }
})

var LabelPanel = React.createClass({
	getDefaultProps: function() {
        return {
            data:[]
        };
    },

	render:function(){
		const self = this
		const listItem = _.map(this.props.data, function(item){
			return (
				<div className="ui label" key={item.key}>
	    			{item.key}
	    			<div className="detail">{item.value}</div>
	    			<i className="delete icon" onClick={self.props.delete.bind(null, item, 'tag')} style={item.canDelete?{}:{display:'none'}}></i>
	    		</div>
			)
		})
		// console.log(listItem, this.props.data)
		return(
			<div className="ui list">{listItem}</div>
		)
	}
})

var TablePanel = React.createClass({
	getDefaultProps: function() {
        return {
            data:[]
        };
    },

	render:function(){
		const self = this
		const listItem = _.map(this.props.data, function(item){
			return (
				<tr key={item.key}>
					<td className="collapsing" style={{maxWidth:40+'%'}}>{item.key}</td>
					<td>{item.value}</td>
					<td><i className="delete icon" onClick={self.props.delete.bind(null, item, 'row')} style={item.canDelete?{}:{display:'none'}}></i></td>
				</tr>
			)
		})

		return(
			<table className="ui very basic celled table">
                <thead><tr><th><i className="folder icon"></i>属性名</th><th><i className="folder icon"></i>属性值</th><th className="collapsing floated right"></th></tr></thead>
                <tbody>{listItem}</tbody>
            </table>
		)
	}
})

var TabPanel = React.createClass({
	getDefaultProps: function() {
        return {
            data:[],
            onCommentChange:function(){},
            edit:function(){},
            delete:function(){}
        };
    },

    activeNew:true,

	componentDidUpdate:function(prevProps, prevState){
		var that = this
		$('.menu a.item').tab({
			onVisible:function(path){
				// console.log(path)
				if(path=='comment'){
					that.activeNew = false	
				}else{
					that.activeNew = true
				}
			}
		})
	},

	commentChange:function(event){
		this.props.onCommentChange(event.target.value)
	},

	render:function(){
		const self = this
		var listTitle = []
		var listItem = []
		var id = new Date().getTime()
		var hasActive=false
		var comment=''
		_.map(this.props.data, function(item){
			if(item.key=='comment'){
				comment = item.value
				return
			}
			var cName,acName
			if(item.canEdit){
				cName='ui attached segment'
			}else{
				cName='ui segment'
			}
			if(item.active && self.activeNew){
				// debugger
				hasActive=true
				acName='ui bottom attached tab segment active'
				listTitle.push(<a key={'a'+id+item.key} className="item active" data-tab={item.key}>{item.key}</a>)
			}else{
				acName='ui bottom attached tab segment'
				listTitle.push(<a key={'a'+id+item.key} className="item" data-tab={item.key}>{item.key}</a>)
			}
			listItem.push(
				<div key={'t'+id+item.key} className={acName} data-tab={item.key}>
					<div className="ui two top attached tiny buttons" style={item.canEdit?{}:{display:'none'}}>
						<button className="ui toggle button" onClick={self.props.edit.bind(null, item)}>编辑</button>
						<button className="ui negative button" onClick={self.props.delete.bind(null, item, 'tab')}>删除</button>
					</div>
					<pre className={cName} style={{whiteSpace:'pre-wrap'}}>{item.value}</pre>
				</div>
			)
		})

		return(
			<div>
				<div ref="tabs" className="ui top attached tabular menu">
					<a className={hasActive?"item":'item active'} data-tab="comment">备注</a>
					{listTitle}
				</div>
				<div className={hasActive?'ui bottom attached tab segment':'ui bottom attached tab segment active'} data-tab="comment">
					<div className="ui form">
						<div className="field">
							<textarea defaultValue={comment} onChange={this.commentChange} placeholder="第一句话请简单概括文件的可识别之处"></textarea>
						</div>
					</div>
				</div>
				{listItem}
			</div>
		)
	}
})

var UserDefinedProperties = React.createClass({
	getDefaultProps: function() {
        return {
            data:[],
            onDataChange:function(){},
        };
    },

	getInitialState: function() {
		var st = this.propsToStates(this.props.data)
		this.props.onDataChange(this.allProps)
		return st
	},

	allProps:{},

	propsToStates:function(props){
		var tags=[]
		var tabs=[]
		var rows=[]
		if(props.data){
			this.allProps = {}
			for(var i in props.data){
				var item = props.data[i]
				this.allProps[item.key]=item
			}
			
			for(var j in this.allProps){
				var p = this.allProps[j]
				var prop = {key:p.key, value:p.value, canDelete:p.canDelete, canEdit:p.canEdit}
				this.allProps[p.key] = prop
				if(p.key=='comment' || p.tab){
					tabs.push(prop)
					continue
				}
				var lenKey = displayLen(p.key)
				var lenValue = displayLen(p.value)
				if(lenKey < 12 && lenValue < 16){
					tags.push(prop)
				}else if(lenValue>80){
					prop.active = false
					tabs.push(prop)
				}else{
					rows.push(prop)
				}
			}
		}
		return {
			tag:tags,
			tab:tabs,
			row:rows,
			edit:{
				modal:false,
				key:'',
				value:'',
			}
		}
	},

	componentWillReceiveProps(newProps) {
	    this.setState(this.propsToStates(newProps))
	    this.props.onDataChange(this.allProps)
	},

	componentDidUpdate:function(prevProps, prevState){
   		
    },

	newUserProperty:function(key,value,fromModal,old){
		var that = this
		
		this.setState({edit:{modal:false,key:'',value:''}})
		if(key && value){
			if(key=='comment'){
				$(this.refs.sameKeyWarn).modal('show')
				return
			}
			if(old){
				this.deleteItem(old, 'tab')
			}
			if(key in this.allProps){
				$(this.refs.sameKeyWarn).modal('show')
				return
			}
			
			var lenKey = displayLen(key)
			var lenValue = displayLen(value)
			var prop = {key:key, value:value, canDelete:true}
			this.allProps[key] = prop
			if(lenKey < 12 && lenValue < 16){
				this.setState({tag:this.state.tag.concat([prop])})
			}else if(fromModal && lenValue>80){
				prop.canEdit = true
				prop.active = true
				var tabs = _.map(this.state.tab,function(item){
					item.active=false
					return item
				})
				tabs.push(prop)
				this.setState({tab:tabs})
			}else{
				this.setState({row:this.state.row.concat([prop])})
			}
			this.props.onDataChange(this.allProps)
		}
	},

	deleteItem:function(item, from){
		delete this.allProps[item.key]
		// console.log(item, from)
		// debugger
		if(from){
			var newState = {}
			newState[from] = _.reject(this.state[from], function(i){return i.key==item.key})
			this.setState(newState)
		}
	},

	onDeleted:function(item, from){
		this.deleteItem(item, from)
		this.props.onDataChange(this.allProps)
	},

	wantUpdate:function(item){
		this.setState({edit:{modal:true, key:item.key, value:item.value}})
	},

	onCommentChange:function(value){
		this.allProps.comment = value
		this.props.onDataChange(this.allProps)
	},

    render: function(){
        return (
            <div className="ui basic segment">
                <InputPanel callback={this.newUserProperty} edit={this.state.edit}/>
                <LabelPanel ref="labels" delete={this.onDeleted} data={this.state.tag} />
                <TablePanel ref="tables" delete={this.onDeleted} data={this.state.row} />
                <TabPanel   ref="tabs" delete={this.onDeleted} edit={this.wantUpdate} data={this.state.tab} onCommentChange={this.onCommentChange} />

				<div ref="sameKeyWarn" className="ui basic modal">
					<div className="ui icon header">
						<i className="clone icon"></i>
						属性名称重复
					</div>
					<div className="content" style={{textAlign: 'center'}}>
						<p>每个文件中不能包含多个相同名称的属性，请您先删除重名属性后再添加。</p>
					</div>
					<div className="actions">
						<div className="ui green ok inverted button">
							<i className="checkmark icon"></i>
							好的
						</div>
					</div>
				</div>

            </div>
        );
    }

});



// var LabelPanel = React.createClass({

// 	getInitialState: function() {
// 		return {
// 			data:{}
// 		}
// 	},

// 	addTag:function(key, value, canDelete){
// 		if(key in this.state.data){
// 			return false
// 		}else{
// 			var m = {}
// 			m[key]={key:key, value:value,canDelete:canDelete}
// 			var mm = _.extend(m, this.state.data)
// 			this.setState({data:mm})
// 			return true
// 		}
// 	},

// 	delete(item){
// 		var newState = _.omit(this.state.data, item.key)
// 		this.setState({data: newState})
// 	},

// 	render: function(){
// 		const listItem = _.map(_.values(this.state.data), function(item){
// 			return (
// 				<div className="ui label">
// 	    			{item.key}
// 	    			<div className="detail">{item.value}</div>
// 	    			<i className="delete icon" onClick={this.delete.bind(this, item)} style={item.canDelete?{}:{display:'none'}}></i>
// 	    		</div>
// 			)
// 		})
// 		return(
// 			<div className="ui list">{this.state.data?'暂无标签':listItem}</div>
// 		)
// 	}

// })