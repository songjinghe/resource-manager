var SearchPanel = React.createClass({
	getDefaultProps: function() {
        return {
        	onChange:function(){},
        	onSubmit:function(){}
        }
    },
	incSearch:function(){
		var curTxt = this.refs.searchTxt.value
		this.props.onChange(curTxt)
	},
	enterSearch:function(event){
		if(event.keyCode == 13){
	    	var curTxt = this.refs.searchTxt.value
			this.props.onSubmit(curTxt)
		}
	},
	advancedSearch:function(){},
	render:function(){
		return(
			<div className="ui fluid action input">
			    <input ref="searchTxt" type="text" placeholder="在这里搜索文件..." onChange={this.enterSearch} onKeyPress={this.incSearch}/>
			    <button type="submit" className="ui button" onClick={this.advancedSearch}>高级搜索</button>
			</div>
		)
	}
})

var ResultItem = React.createClass({
	getDefaultProps: function() {
        return {
        	data:{},
        	highLights:{}
        }
    },

    highlightComment:function(comment, highlightList, length){
    	if(comment && highlightList){
    		var positions = _.filter(_.map(highlightList, function(item){
    			return comment.indexOf(item)
    		}),function(item){
    			return item!=-1
    		})
    		var min = _.min(positions)
    		var max = _.max(positions)
    		var prefix, postfix
    		if(min-10>0){
    			prefix = '... '
    			if(length){
	    			comment = comment.substr(min-10, length)
	    			postfix = ' ...'
	    		}else{
	    			comment = comment.substr(min-10)
	    			postfix = ''
	    		}
    		}else{
    			prefix = ''
    			postfix = ''
    		}

    		var re = new RegExp(highlightList.join("|"),"gi");

	    	return prefix + comment.replace(re, function(matched){
				return '<span style="background-color:#0FF">'+matched+'</span>';
			}) + postfix
    	}else{
    		if(length){
	    		return comment.slice(0, length) + ' ...'
    		}else{
    			return comment
    		}
    	}
    },

    highlightEZPComment:function(comment, highlightList, length){
    	var ezpComment = JSON.parse(comment)
    	var ezpCommentStr = _.values(ezpComment).join(' ')
    	return highlightComment(ezpCommentStr, highlightList, length)
    },

	render:function(){
		function labelTpl(key,value,id){
			return (
				<div key={id?id:''} className='ui tiny label' title={key} dangerouslySetInnerHTML={{__html:value}}></div>
			)
		}


    	var sysProperties = {}
    	var userProperties = {}

    	var item = _.clone(this.props.data)
    	for(var k in this.props.data){
    		switch(k){
			case 'comment':
				if(item[k].length==0) break;
				item[k] = this.highlightComment(item[k], this.props.highLights['comment'], 80)
				break
			case 'fileEntity':
				if(item[k].id==0) break;
				sysProperties['创建时间']=item[k].createTime,
	    		sysProperties['最后修改时间']=item[k].lastModifiedTime,
	    		sysProperties['唯一标识'] = hashToShort(item[k].sha512)
	    		sysProperties['元信息更新时间']=item[k].metaUpdateTime,
	    		sysProperties['大小'] = size2str(item[k].size)
	    		for(var i in item[k].properties){
	    			var p = item[k].properties[i]
	    			if(p.name=='ezpComment'){
	    				if('marker' in this.props.highLights){
			    			item['ezpComment'] = this.highlightEZPComment(p.value, this.props.highLights['marker'], 80)	    					
	    				}else{
	    					item['ezpComment'] = _.values(JSON.parse(p.value))
	    				}
	    			}else{
	    				userProperties[p.name] = p.value
	    			}
	    		}
				break
			case 'properties':
				for(var i in item[k]){
					var p = item[k][i]
	    			userProperties[p.name] = p.value
				}
				break
    		}
    	}

    	const sysLabels = _.values(_.mapObject(sysProperties,function(val,k){return labelTpl(k,val,k)}))

		const self = this
		const highLights = this.props.highLights
		
		const userLabels = _.values(_.mapObject(userProperties, function(value, key){
			if(key in highLights){
				const re = new RegExp(highLights[key].join("|"),"gi");
				return labelTpl(key, item.value.replace(re, function(matched){
					return '<span style="backgroundColor:#0FF">'+matched+'</span>'
				}), key)
			}else{
				return labelTpl(key, value, key)
			}
		}))
		
		const Description = function(props){
			return (
				<div className="description">
					<div className='ui tiny label' style={props.comment?{}:{display:'none'}}>备注</div>
	    			<p style={{display:'inline'}} dangerouslySetInnerHTML={{__html:props.comment}}></p>
	    			<div className='ui tiny label' style={props.ezpComment?{}:{display:'none'}}>内部注释</div>
	    			<p style={{display:'inline'}} dangerouslySetInnerHTML={{__html:props.ezpComment}}></p>
				</div>
			)
		}

		return(
			<div className="item" style={{borderBottomWidth:1,borderBottomStyle:'solid',borderBottomColor:'#999',paddingTop:0,paddingLeft:4,paddingRight:4,paddingBottom:8}}>
				<div className="content">
					<a className="header" 
					   href={"/browse/path/"+this.props.data.path}
					   target="_blank">
						<i className="file archive outline icon"></i>
						{getFileNameFromPath(this.props.data.path)}
					</a>
					<div className="meta ui tiny labels">
						<span><i className="folder icon"></i>{getFilePath(this.props.data.path)}</span>
						{sysLabels}
					</div>
					<Description comment={item.comment} ezpComment={item.ezpComment} />
					<div className="extra">
						{userLabels}
					</div>
				</div>
			</div>
		)
	}
})

var ResultPanel = React.createClass({
	getDefaultProps: function() {
        return {
        	loading:true,
        	data:[],
        	highLights:{}
        }
    },
	render:function(){
		var self = this
		var items = _.map(this.props.data, function(item){
			//console.log(item)
			return <ResultItem key={item.id} data={item} highLights={self.props.highLights}/>
		})
		console.log('render items')
	    return(
	    	<div className={this.props.loading?"ui items loading":"ui items"}>{items}</div>
	    );
	}
})

var Main = React.createClass({

	lastTxt:'nothing',
	curTxt:'',

	getInitialState: function() {
		var that=this
		var count = 0
        function bindJava(){
            if(window.JavaApp){
                that.setState({JavaApp:true})
                console.log("java app bind in "+count*100+"ms! will choose file from javaFX without upload")
            }else{
                if(count==0){
                	console.log("window.JavaApp not bind!")
                }
                count++
                setTimeout(bindJava,100)
            }
        }

        bindJava()
        setInterval(this.runSearch, 600)

        return {
        	recent:true, // waiting, done
        	JavaApp:false,
        	results:[],
        	highLights:{},
        	loading:true
        };
    },

	searchTxtChange:function(curTxt){
		this.curTxt = curTxt
	},

	searchSubmit:function(curTxt){
		this.curTxt = curTxt
	},

	toHighLight:function(curTxt){
    	var result = {
    		comment:[]
    	}
    	if(curTxt){
    		var seg = curTxt.split(/\s+/)
	    	// console.log(seg)
	    	var pairs = _.filter(seg, function(item){
	    		var i = item.toUpperCase()
	    		return !(i=='AND' || i=='OR')
	    	})
	    	_.each(pairs, function(item){
	    		var tmp = item.split(':')
	    		if(tmp.length>1){
	    			if(tmp[0] && tmp[1]){
	    				if(!(tmp[0] in result)){
			    			result[tmp[0]]=[]
	    				}
		    			result[tmp[0]].push(tmp[1])
	    			}
	    		}else{
	    			result['comment'].push(item)
	    		}
	    	})
	    	// console.log(result)
	    	return result
    	}else{
    		return {}
    	}
    },

	runSearch:function(){
		if(this.lastTxt!=this.curTxt){
			this.lastTxt = this.curTxt
	    	this.setState({'loading':true})
	    	var that = this
			if(this.curTxt){
				var highLights = this.toHighLight(this.curTxt)
				$.get('/json/search/'+this.curTxt,function(data){
					that.setState({results:data, recent:false, loading:false, highLights:highLights})
				})
			}else{
				$.get('/json/search/latest', function(data){
					that.setState({results:data, recent:true, loading:false, highLights:{}})
				});
	    	}
		}
	},

    render:function(){
        return (
        	<div className="ui main text container" style={{maxWidth:960, backgroundColor: '#f9f9f9'}}>
				<SearchPanel onChange={this.searchTxtChange} onSubmit={this.searchSubmit} />
				
				<a className="ui right floated button" target="_blank" href="/import">添加文件</a>

			    <div className="ui horizontal divider">{this.state.recent?'最近文件':'搜索结果'}</div>

				<ResultPanel data={this.state.results} highLights={this.state.highLights} loading={this.state.loading} />
			</div>
        );
    }
})


ReactDOM.render(
    <Main />,
    document.getElementById('main')
);