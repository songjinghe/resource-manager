const FileTreeNode = (props)=>{
	var keys = _.keys(props.children).sort()
	var children = _.map(keys, function(key){
		var item = props.children[key]
		var icon
		if(_.keys(item).length==0){
			icon = fileTypeIcon(key)+' icon'
			return (
				<div className="item" key={key}>
					<i className={icon}></i>
					<div className="content">
						<div className="description">{key}</div>
					</div>
				</div>
			)
		}else{
			icon = 'folder open disabled icon'
			return (
				<div className="item" key={key}>
					<i className={icon}></i>
					<div className="content">
						<div className="description">{key}</div>
						<FileTreeNode children={item} />
					</div>
				</div>
			)
		}
	})

	return(
		<div className="ui list">{children}</div>
	)
}

var RelFiles = React.createClass({

	addRelFile:function(fileList){
		var tree = groupFileByFolderIter(fileList)
		this.setState({tree: tree})
	},

    render: function(){
    	var tree = groupFileByFolderIter(this.props.fileList)
    	//debugger
        return (
            <div className="ui basic segment">
                <div id="file-tab-head" className="ui top attached tabular menu">
                    <a className="item active" data-tab="tree-view">树状视图</a>
                    <a className="item" data-tab="list-view">列表视图</a>
                </div>
                <div className="ui bottom attached tab segment active" data-tab="tree-view">
                    <FileTreeNode children={tree} />
                </div>
                <div className="ui bottom attached tab segment" data-tab="list-view">
                    <div id="rel-files" className="ui relaxed divided list"></div>
                </div>
            </div>
        );
    }

});