var MessageBar = React.createClass({
	getDefaultProps: function() {
        return {
        	state:'busy',
        	close:true,
        	delay:0,
        	text:''
        }
    },

    getInitialState:function(){
    	return {
    		show:true
    	}	
    },

    componentDidUpdate:function(prevProps, prevState){
		const self = this
		if(!this.state.show) return
		$(self.refs.Msg).show()
		//console.log(this.props)
		if(this.props.delay){
			setTimeout(function(){
				self.setState({show:false})
				$(self.refs.Msg).slideUp()
			}, parseInt(this.props.delay)*1000)
		}
    },

    close:function(){
    	$(this.refs.Msg).slideUp()
    },

	render:function(){
		var boxClass, iconClass, title
		switch(this.props.state){
			case "busy":
				boxClass = "ui icon attached info compact message"
				iconClass = "notched circle loading icon"
				title  = "请稍等片刻"
				break;
			case "done":
				boxClass = "ui icon attached success compact message"
				iconClass = "checkmark icon"
				title = "完成"
		}
		
	    return(
	    	<div ref="Msg" className={boxClass} style={this.state.show?{}:{display:'none'}}>
	            <i className={iconClass}></i>
	            <i className="close icon" style={this.props.close?{}:{display:'none'}} onClick={this.close}></i>
	            <div className="content">
	                <div className="header">{title}</div>
	                <p>{this.props.text}</p>
	            </div>
	        </div>
	    );
	}
})


var AskEZPFrameRate = React.createClass({
	getDefaultProps: function() {
        return {
        	onFrameRate:function(frameRate){},
        	show:false
        };
    },

	getInitialState: function() {
        return {
        	frameRate:'25',
        	chooseOther:false
        }
    },

    confirm:false,

    componentDidMount:function(){
   		$('.ui.radio.checkbox').checkbox()
    },

	componentDidUpdate:function(prevProps, prevState){
		$('.ui.radio.checkbox').checkbox()
		const self = this
		if(!this.props.show) return
		$(this.refs.askFrameRate).modal({
			onApprove:function(){
				var frameRate = self.state.frameRate
				console.log(frameRate)
				if(frameRate){
					self.props.onFrameRate(frameRate)
				}
			}
		}).modal('show')
    },

    choose:function(frameRate){
    	// console.log(frameRate)
    	// debugger
    	if(frameRate!='other'){
			this.setState({frameRate:frameRate,chooseOther:false})
		}
    },

    enter:function(event){
    	var frameRate = event.target.value
    	// console.log(frameRate)
    	// debugger
    	if(this.state.chooseOther){
    		this.setState({frameRate:frameRate})
    	}
    },

    focus:function(){
    	this.setState({chooseOther:true})
    },

	render:function(){
		const self = this
		const itemList = _.map([23.976, 25, 29.97, 50, 59.94],function(frameRate){
			return (
				<div key={frameRate} className="field">
					<div className="ui radio checkbox" onClick={self.choose.bind(null,frameRate)}>
						<input type="radio" name="frameRate" value={frameRate} className="hidden" defaultChecked={frameRate==25}/>
						<label>{frameRate}</label>
					</div>
				</div>
			)
		})


		return (
			<div ref="askFrameRate" className="ui modal">
				<i className="close icon"></i>
				<div className="header">
					请选择帧率（单位：FPS）
				</div>
				<div className="content">
					<div className="ui form">
						<div className="grouped fields">
							<label>这是由系统识别文件的帧率。如不正确，请改为正确的帧率</label>
							{itemList}
							<div className="field">
								<div className="ui radio checkbox" onClick={this.choose.bind(null,'other')} >
									<input type="radio" name="frameRate" value="other" className="hidden" checked={this.state.chooseOther}/>
									<label>其他</label>
								</div>
								<div className="ui icon input">
									<input ref="userEnteredFrameRate" type="text" placeholder="帧率（单位：帧数每秒）" onFocus={this.focus} onChange={this.enter} />
									<i className="file video outline icon"></i>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div className="actions">
					<div className="ui cancel button">取消</div>
					<div className="ui positive approve right labeled icon button">
						确定
						<i className="checkmark icon"></i>
					</div>
				</div>

			</div>
		)
	}
})



var Main = React.createClass({

	getInitialState: function() {
        return {
        	phase:'init', // pathReady, infoReady, hashReady, finish
        	fileProperties:[],
        	askFrameRate:false,
        	msg:{},
        	path:''
        };
    },

    addSysProp:function(key,value){
		var prop = {
			key:key,
			value:value,
			canEdit:false,
			canDelete:false
		}
		this.setState({fileProperties:this.state.fileProperties.concat([prop])})
    },

	getFileInfo:function(path){
		var that = this
		function add(key,value){
			return {key:key,value:value, canEdit:false, canDelete:false}
		}
		$.get('/json/import/'+encodeURIComponent(path)+'/info', function(data){
			that.fileObj.size = data.size
			that.fileObj.path = data.path
			that.fileObj.createTime = data.createTime
			that.fileObj.lastModifiedTime = data.lastModifiedTime
			that.addSysProp('文件大小', size2str(data.size))
			that.addSysProp('后缀名',  getFileExt(path))
			that.addSysProp('创建时间', moment(data.createTime).format('lll')+' '+getTimeZone()+' ('+moment(data.createTime).fromNow()+')')
			that.addSysProp('最后修改时间', moment(data.lastModifiedTime).format('lll')+' '+getTimeZone()+' ('+moment(data.lastModifiedTime).fromNow()+')' )
			if(data.ezpInfo){
				that.fileObj.ezpComment = data.ezpInfo.comments
				that.fileObj.relFiles = data.ezpInfo.materials
				that.setState({askFrameRate:true,fileList:data.ezpInfo.materials})
			}
			that.setState({phase:'infoReady', path:data.path})
			that.getFileHash(path)
		})
	},

	uploadFileCallBack: function(path){
		// $('#header-below').css('margin-top', parseInt($('#header h2').outerHeight())+'px')
		console.log(path)
		this.setState({phase:'pathReady'})
		this.getFileInfo(path)
	},
	getFileHash:function(path){
		this.setState({msg:{
			state:'busy',
			text:'正在计算文件的唯一识别/校验码，文件越大需时越长',
			close:false
		}})
		var self = this
		$.get('/json/import/'+encodeURIComponent(path)+'/hash', function(data){
			if(data.exist){
				self.onEntityExist(data.sha512)
			}
			self.fileObj.sha512 = data.sha512
			self.fileObj.md5head = data.md5head
			self.addSysProp('唯一标识', hashToShort(data.sha512))
			if(data.md5head){
				self.addSysProp('校验码', hashToShort(data.md5head))
			}
			self.setState({
				phase:'hashReady',
				canSubmit:true,
				msg:{
					state:'done',
					text:'文件识别码与校验码已计算完成，填写完下面的自定义属性就可以提交了',
					close:true,
					delay:10
				}
			})

			
		})
	},

	onEntityExist:function(sha512){
		document.location.replace('/')
	},
	
	fileObj:{},
	visiableProps:{},

	propertiesUpdate:function(properties){
		//console.log(properties)
		this.visiableProps = properties
	},

	onFrameRate:function(frameRate){
		// console.log(frameRate)
		frameRate = parseFloat(frameRate)
		var tmp=this.state.fileProperties.slice()
		tmp.push({key:'帧率',value:frameRate,canDelete:true,canEdit:false})
		tmp.push({key:'EZP Markers', value:getEzpCommentStr(this.fileObj.ezpComment,frameRate), canEdit:false, canDelete:false, tab:true})
		this.setState({fileProperties:tmp, askFrameRate:false})
	},

	onSubmit:function(){
		//console.log(this.fileObj, this.visiableProps)
		if(this.state.phase=='hashReady'){
			this.fileObj = _.extend(this.fileObj, _.mapObject(_.omit(this.visiableProps,function(value, key, object){
				var notSubmit=['文件大小','创建时间','最后修改时间','唯一标识','EZP Markers']
				return notSubmit.indexOf(key)!=-1
			}),function(val, key){
				return val.value
			}))
			this.fileObj.ezpComment = JSON.stringify(this.fileObj.ezpComment)
			this.showWaitingDialog()
			$.ajax({
			    type: 'POST',
			    url: '/post/import/new',
			    data: JSON.stringify(this.fileObj),
			    success: function(data){
			    	console.log(data)
					window.close()
				},
			    contentType: "application/json",
			    dataType: 'text'
			});
		}
	},

	showWaitingDialog: function(){
		if(window.JavaApp){
			window.JavaApp.waiting()
		}else{
			$(this.refs.loading).dimmer('show')
		}
	},

    render:function(){
        return (
            <div className="ui main text container" style={{maxWidth:960, backgroundColor: '#f9f9f9'}}>
                <FileUpload path={this.state.path} onUploadDone={this.uploadFileCallBack} canSubmit={this.state.canSubmit} onSubmit={this.onSubmit}/>
                <div>
	                {this.state.phase=='infoReady'? <MessageBar ref="msg" state={this.state.msg.state} text={this.state.msg.text} close={this.state.msg.close} delay={this.state.msg.delay} /> :''}
                    <div className="ui horizontal divider">自定义属性</div>
	                <UserDefinedProperties ref="properties" data={this.state.fileProperties} onDataChange={this.propertiesUpdate}/>
                    <div className="ui horizontal divider">相关文件</div>
                    {(this.state.phase=='infoReady' || this.state.phase=='hashReady')? 
                    	<RelFiles fileList={this.state.fileList} /> : ''}
                </div>

                <div ref="loading" className="ui page dimmer">
					<div className="ui massive text loader">提交中，請稍等（稍后自动关闭）....</div>
				</div>

				<AskEZPFrameRate show={this.state.phase=='infoReady'} onFrameRate={this.onFrameRate}/>
				
            </div>
        );
    }
})


ReactDOM.render(
    <Main />,
    document.getElementById('main')
);