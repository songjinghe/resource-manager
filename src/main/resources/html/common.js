if((typeof moment)!='undefined') moment.locale('zh-cn')

function size2str(size){
	var sb='';
	var remain=parseInt(size);
	var oneKB=1024;
	var oneMB=oneKB*1024;
	var oneGB=oneMB*1024;
	var GB=0;
	var MB=0;
	var KB=0;
	var B=0;
	if(remain>oneGB) GB=parseInt(remain/oneGB);
	remain = remain % oneGB;
	if(remain>oneMB) MB=parseInt(remain/oneMB);
	remain = remain % oneMB;
	if(remain>oneKB) KB=parseInt(remain/oneKB);
	B = remain % oneKB;
	if(GB>0){
	    return (size/oneGB).toFixed(2)+"GB";
	}else if(MB>0){
	    if(MB>100) {
		return MB + "MB";
	    }else if(MB>10) {
		return (size/oneMB).toFixed(1)+"MB";
	    }else{
		return (size/oneMB).toFixed(2)+"MB";
	    }
	}else if(KB>0){
	    if(KB>10){
		return (size/oneKB).toFixed(1)+"KB";
	    }else{
		return (size/oneKB).toFixed(2)+"KB";
	    }
	}else{
	    return B+"Bytes";
	}

}

function getTimeZone(){
	var offset = new Date().getTimezoneOffset()
	if(offset==-480){
		return '北京时间'
	}else{
		var zone = offset/60
		return 'GMT'+(zone>0?'-':'+')+Math.abs(zone)
	}
}

function getFileExt(name){
	return name.slice(name.lastIndexOf('.')).toUpperCase()
}

function getFileNameFromPath(path){
	if(path.indexOf('\\')>=0){ //from windows
		return path.slice(path.lastIndexOf('\\')+1)
	}else{
		return path.slice(path.lastIndexOf('/')+1)
	}
}

function getFilePath(path){
	if(path.indexOf('\\')>=0){ //from windows
		return path.slice(0, path.lastIndexOf('\\'))
	}else{
		return path.slice(0, path.lastIndexOf('/'))
	}
}

function hashToShort(hash){
	if(hash.length>17){
		return hash.slice(0,7)+'...'+hash.slice(-7)
	}else{
		return hash
	}
}

function groupFileByFolderIter(fileList){
	var isWindows = true
	function splitFilePath(path){
		var lst
		if(path.indexOf('\\')!=-1){
			lst = path.split('\\')
			if(lst.length>1 && lst[0]=='' && lst[1]==''){
				lst[2] = '\\\\'+lst[2]
				return _.rest(lst, 2)
			}else{
				return lst
			}
		}else{
			isWindows=false
			lst = path.split('/')
			if(lst.length>1 && lst[0]==''){
				lst[1] = '/'+lst[1]
				return _.rest(lst, 1)
			}else{
				return lst
			}
		}
	}
	function mergeTree(tree){
		var grandpa = tree
		for(var i in tree){
			var fatherKey = i
			var father = grandpa[fatherKey]
			var sonKeys = _.keys(father)
			while(sonKeys.length==1){
				var son = father[sonKeys[0]]
				delete grandpa[fatherKey]
				fatherKey += (isWindows?'\\':'/')+sonKeys[0]
				grandpa[fatherKey] = son
				father = son
				sonKeys = _.keys(father)
			}
			if(sonKeys.length>1){
				grandpa[fatherKey] = mergeTree(father)
			}
		}
		return tree
	}
	var tree = {}
	for(var j in fileList){
		var cur = tree
		var file = splitFilePath(fileList[j])
		var key = file[0]
		var i=0
		while((key in cur)&&(i<file.length)){
			cur = cur[key]
			i++
			key = file[i]
		}
		while(i<file.length){
			cur[key] = {}
			cur = cur[key]
			i++
			key = file[i]
		}
	}
	return mergeTree(tree)
}


function getEzpCommentStr(comments, frameRate){
	function fill2(num){
		return (num<10?'0':'') + num
	}
	function getTimeFromPos(frameRate, pos){
		var totalSeconds = parseInt(pos/frameRate)
		var hours = parseInt(totalSeconds/3600)
		var minutes = parseInt((totalSeconds%3600)/60)
		var seconds = parseInt(totalSeconds%60)
		var remain = pos - parseInt(totalSeconds*frameRate)
		return fill2(hours)+':'+fill2(minutes)+':'+fill2(seconds)+':'+fill2(remain)
	}
	var i=1
	var result = ''
	frameRate = parseFloat(frameRate).toFixed(2)
	for(var pos in comments){
		result += (fill2(i)+', '+ getTimeFromPos(frameRate,pos) + '   ' + comments[pos] + '\n')
		i++
	}
	return result
}


function fileTypeIcon(name){
	var ext = getFileExt(name).substring(1)
	switch(ext){
		case 'MOV':
		case 'AVI':
		case 'M2V':
		case 'MPA':
		case 'MPG': return 'file video outline'
		case 'ZIP':
		case 'RAR': return 'file archive outline'
		case 'MP3':
		case 'WMA':
		case 'WAV':
		case 'AC3':
		case 'AAC': return 'file audio outline'
		case 'DOC':
		case 'DOCX': return 'file word outline'
		case 'PDF': return 'file pdf outline'
		case 'JPEG':
		case 'JPG':
		case 'PNG':
		case 'BMP':
		case 'PSD': return 'file image outline'
		default: return 'file outline'
	}
}

function displayLen(txt){
	return encodeURIComponent(txt).replace(/%..%..%../g, 'XX').length
}