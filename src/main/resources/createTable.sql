-- this file is for

create table if not exists file_entity (
    id             integer NOT NULL PRIMARY KEY,
    size           integer,
    sha512         character(128),
    md5_head       character(32),
    create_t       datetime,
    update_t       datetime
);

create table if not exists res_file (
    id             integer NOT NULL PRIMARY KEY,
    entity_id      integer,
    online         boolean,
    meta_update_t  datetime DEFAULT CURRENT_TIMESTAMP,
    path           text,
    comment        text
);

create table if not exists properties(
    id             integer NOT NULL PRIMARY KEY,
    type           character(16),
    target_id      integer not null,
    name           text,
    value          text
);

create table if not exists relation(
    id             integer NOT NULL PRIMARY KEY,
    from_file_id   integer not null,
    to_file_id     integer not null,
    type           character(16),
    comment        text
);

CREATE UNIQUE INDEX if not exists fast_sha512 ON file_entity(sha512);
-- CREATE UNIQUE INDEX if not exists file_rel ON relation(from_file_id, to_file_id, type);
CREATE INDEX if not exists file_rel_id0 ON relation(from_file_id);
CREATE INDEX if not exists file_rel_id1 ON relation(to_file_id);
CREATE INDEX if not exists index_res_file_entity_id ON res_file(entity_id);
CREATE INDEX if not exists index_property_target_id ON properties(target_id);