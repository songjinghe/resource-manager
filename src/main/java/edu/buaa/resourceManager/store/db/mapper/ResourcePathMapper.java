package edu.buaa.resourceManager.store.db.mapper;

import edu.buaa.resourceManager.vo.CustomProperty;
import edu.buaa.resourceManager.vo.ResourcePath;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.apache.ibatis.plugin.Intercepts;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by sjh on 17-7-1.
 */

public interface ResourcePathMapper
{
    @Select("SELECT * FROM res_file ORDER BY meta_update_t ASC LIMIT 20")
    @Results(value={
            @Result(property = "id", column = "id"),
            @Result(property = "metaUpdateTime",column = "meta_update_t"),
            @Result(property = "properties", column = "id", javaType=List.class, many=@Many(select="getPropertyByPathId", fetchType=FetchType.EAGER)),
            @Result(property = "fileEntity", column = "entity_id", one=@One(select = "edu.buaa.resourceManager.store.db.mapper.FileEntityMapper.getByIdWithPropNoPath"))
    })
    List<ResourcePath> getLatest();

    @Select("SELECT * FROM res_file WHERE id = #{id}")
    ResourcePath getById(int id);

    @Select("SELECT * FROM res_file, ")
    ResourcePath getBySha512(String sha512);

    @Insert("INSERT into res_file (path, online, meta_update_t, comment, entity_id) " +
            "VALUES(#{path},#{online},#{metaUpdateTime},#{comment},#{fileEntity.id})")
    @Options(useGeneratedKeys=true)
    int create(ResourcePath path);

    @Select("SELECT id, path, online, meta_update_t, comment FROM res_file WHERE entity_id=#{entityId}")
    @Results(value={
            @Result(property="properties",
                    column="id",
                    javaType=List.class,
                    one=@One(select="getPropertyByPathId",
                    fetchType=FetchType.EAGER))
    })
    List<ResourcePath> getByEntityId(int entityId);

    @Select("SELECT * FROM properties WHERE type='path' AND target_id=#{id}")
    List<CustomProperty> getPropertyByPathId(int id);


    @Insert("INSERT into properties (`type`, target_id, name, value) " +
            "VALUES('path', #{targetId}, #{name}, #{value})")
    @Options(useGeneratedKeys=true) //https://stackoverflow.com/questions/4283159
    int addProperty(CustomProperty property);


//    @Insert("INSERT into res_file (path, online, comment) " +
//            "SELECT #{path},#{online},#{comment} WHERE NOT EXIST" +
//            "(SELECT 1 FROM res_file WHERE path=#{path})") //https://stackoverflow.com/a/19079810
    @Insert("INSERT INTO res_file (path, entity_id, online, comment, meta_update_t)" +
            "VALUES (#{path}, 0, #{online}, #{comment}, #{metaUpdateTime})")
    @Options(useGeneratedKeys=true) //返回的不是id，而是affectedRowsCount，id直接写到对象里了
    int createWithoutEntity(ResourcePath path);


    @Select("SELECT * FROM res_file WHERE path=#{path}")
    List<ResourcePath> getByPath(String path);

    @Insert("INSERT INTO relation (from_file_id, to_file_id, type, comment)" +
            "VALUES(#{projectId}, #{resourceId}, 'depend', 'AUTO')")
    void addDependency(@Param("projectId") int projectId, @Param("resourceId") int resourceId);
    //多个参数需要前面加Param标记才能使用～～见：https://stackoverflow.com/a/26012256
}
