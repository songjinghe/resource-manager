package edu.buaa.resourceManager.helper;

import java.sql.SQLException;

/**
 * Created by song on 17-6-21.
 */
public class RMException extends Exception
{
    public RMException(Exception e) {
        super(e);
    }
}
