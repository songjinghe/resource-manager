package edu.buaa.resourceManager.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.TimeZone;

/**
 * Created by sjh on 17-5-20.
 */
public class Utils
{
    public static String timestamp2Str(long time)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        return sdf.format(new Date(time));
    }

    public static File getParentDirOfFile(File file) throws IOException {
        if(file.exists()){
            if(file.isDirectory()){
                return file;
            }else{
                return file.getParentFile();
            }
        }else{
            throw new IOException("can not get parent dir of file!");
        }
    }

    public static String size2Str(long size) {
        StringBuilder sb = new StringBuilder();
        long remain=size;
        long oneKB=1024;
        long oneMB=oneKB*1024;
        long oneGB=oneMB*1024;
        long GB=0, MB=0, KB=0, B=0;
        if(remain>oneGB) GB=remain/oneGB;
        remain = remain % oneGB;
        if(remain>oneMB) MB=remain/oneMB;
        remain = remain % oneMB;
        if(remain>oneKB) KB=remain/oneKB;
        B = remain % oneKB;
        if(GB>0){
            return String.format("%.2fGB", size/(oneGB*1f));
        }else if(MB>0){
            if(MB>100) {
                return MB + "MB";
            }else if(MB>10) {
                return String.format("%.1fMB", size / (oneMB * 1f));
            }else{
                return String.format("%.2fMB", size / (oneMB * 1f));
            }
        }else if(KB>0){
            if(KB>10){
                return String.format("%.1fKB", size / (oneKB * 1f));
            }else{
                return String.format("%.2fKB", size / (oneKB * 1f));
            }
        }else{
            return B+"Bytes";
        }
    }

    public static String readStringFromResource(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(Utils.class.getResourceAsStream(path)));
        StringBuilder sb = new StringBuilder();
        String line=null;
        while((line=reader.readLine())!=null){
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }

    public static Logger log = LoggerFactory.getLogger("Amitabha");

    public static String getFileHeadMd5(File file) throws IOException {
        int tenMB = 10*1024*1024;
        byte[] bytesOfFileHead = new byte[tenMB];
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            in.readFully(bytesOfFileHead);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] thedigest = md.digest(bytesOfFileHead);
            in.close();
            return byteArray2Hex(thedigest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public static String uriDecode(String path) {
        try {
            return URLDecoder.decode(path.replace("+", "%2B"), "UTF-8")
                    .replace("%2B", "+");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Logger getDevLogger() {
        return log;
    }


}
