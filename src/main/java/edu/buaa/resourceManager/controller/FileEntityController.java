package edu.buaa.resourceManager.controller;

import edu.buaa.resourceManager.store.JsonStore;
import edu.buaa.resourceManager.store.StoreDB;
import edu.buaa.resourceManager.vo.FileEntity;
import edu.buaa.resourceManager.vo.ResourcePath;
import spark.Route;

import java.util.List;

/**
 * Created by sjh on 17-7-2.
 */
public class FileEntityController
{
    public static Route SameFiles = (request, response) -> {
        String sha512 = request.params(":sha512");
        FileEntity result = StoreDB.getInstance().getFilesBySha512(sha512);
        // result can be null !
        return JsonStore.getInstance().json.toJson(result);
    };
}
