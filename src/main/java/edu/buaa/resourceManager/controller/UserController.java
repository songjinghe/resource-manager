package edu.buaa.resourceManager.controller;

import edu.buaa.resourceManager.helper.Utils;
import org.slf4j.Logger;
import spark.Route;

import java.util.Scanner;

/**
 * Created by song on 17-6-22.
 */
public class UserController
{
    private static Logger log = Utils.getDevLogger();

    public static Route Login = (request, response) -> {
        response.type("text/html");
        return new Scanner(ImportController.class.getResourceAsStream("/html/login.html"), "UTF-8").useDelimiter("\\A").next();
    };
}
