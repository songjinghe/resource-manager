package edu.buaa.resourceManager;

import edu.buaa.resourceManager.helper.Utils;
import edu.buaa.resourceManager.search.Search;
import edu.buaa.resourceManager.server.SparkHttpServer;
import edu.buaa.resourceManager.store.StoreDB;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created by song on 17-5-25.
 *
 * usage:
 * java -jar RM.jar add-file /tmp/file.abc
 * >> {file-id: 1}
 * java -jar RM.jar query-by-path /tmp/file.abc
 * >>
 * java -jar RM.jar link-file /tmp/file.abc /tmp/file2
 */
public class ProgramCMD
{
    public static volatile boolean exit=false;

    public static void main(String[] args)
    {
        Utils.log.trace("hello~");

        StoreDB.getInstance();
//        JsonStore.getInstance();
        Search.getInstance();

        new SparkHttpServer().start();
        Utils.log.trace("server started...");

        if(System.getProperty("GUI")!=null){
            ProgramGUI.startFromCMD(args);
        }else {
            Scanner reader = new Scanner(System.in);  // Reading from System.in
            while (!exit) {
                String cmd = reader.next();
                if ("exit".equals(cmd)) break;
            }
        }

        Utils.log.trace("program shutting down...");
        SparkHttpServer.stopServer();

        try {
            Search.getInstance().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Config.toFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Utils.log.trace("Good Bye~");
    }
}
