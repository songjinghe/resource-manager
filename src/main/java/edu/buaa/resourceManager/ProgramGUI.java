package edu.buaa.resourceManager;

import edu.buaa.resourceManager.helper.Utils;
import edu.buaa.resourceManager.ui.MainWindow;
import javafx.application.Application;
import javafx.stage.Stage;

import java.net.MalformedURLException;

/**
 * GUI of this program.... only a webView ~
 */
public class ProgramGUI extends Application
{
    @Override
    public void start(Stage stage){
        try {
            new MainWindow();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    static void startFromCMD(String[] args) {
        Utils.log.trace("starting GUI...");

        Application.launch(args);

        Utils.log.trace("GUI shutdown.");
    }
}