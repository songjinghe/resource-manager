package edu.buaa.resourceManager.server;


/**
 * Created by sjh on 17-7-2.
 * https://stackoverflow.com/a/29380161
 */
public abstract class BackgroundTask implements Runnable, Comparable<BackgroundTask> {
    private int priority;
    private boolean exit=false;

    protected void setPriority(int priority){
        this.priority = priority;
    }

    protected void setExit(boolean exit) {
        this.exit = exit;
    }

    @Override
    public int compareTo(BackgroundTask other) {
        return Integer.compare(this.priority, other.priority);
    }

    public boolean getExit() {
        return exit;
    }
}
