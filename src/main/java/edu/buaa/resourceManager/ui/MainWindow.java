package edu.buaa.resourceManager.ui;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by sjh on 17-5-30.
 */
public class MainWindow extends WebViewWindow {
    public MainWindow() throws MalformedURLException {
        super(new URL("http://localhost:8888/search"));
//        super(MainWindow.class.getResource("/html/main.html"));
    }
}
