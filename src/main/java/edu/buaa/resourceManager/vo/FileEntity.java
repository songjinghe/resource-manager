package edu.buaa.resourceManager.vo;

import java.sql.Date;
import java.util.List;

/**
 * Created by song on 2017/6/20 0020.
 */
public class FileEntity
{
    private int id;
    private long size;
    private String sha512;
    private String headMd5;
    private Date createTime;
    private Date lastModifiedTime;
    private List<ResourcePath> paths;
    private List<CustomProperty> properties;
    private String comment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getSha512() {
        return sha512;
    }

    public void setSha512(String sha512) {
        this.sha512 = sha512;
    }

    public String getHeadMd5() {
        return headMd5;
    }

    public void setHeadMd5(String headMd5) {
        this.headMd5 = headMd5;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(Date lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public List<ResourcePath> getPaths() {
        return paths;
    }

    public void setPaths(List<ResourcePath> paths) {
        this.paths = paths;
    }

    public List<CustomProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<CustomProperty> properties) {
        this.properties = properties;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileEntity that = (FileEntity) o;

        if (id == that.id){
            return true;
        }else {
            return size == that.size && sha512.equals(that.sha512) && headMd5.equals(that.headMd5);
        }
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (int) (size ^ (size >>> 32));
        result = 31 * result + sha512.hashCode();
        result = 31 * result + headMd5.hashCode();
        result = 31 * result + createTime.hashCode();
        result = 31 * result + lastModifiedTime.hashCode();
        return result;
    }
}
