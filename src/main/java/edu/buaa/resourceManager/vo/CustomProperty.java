package edu.buaa.resourceManager.vo;

import java.io.Serializable;

/**
 * Created by song on 2017/5/22 0022.
 */
public class CustomProperty implements Serializable
{
    int id;
    String type;
    int targetId;
    String name;
    String value;

    public CustomProperty(String key, Object value) {
        this.name = key;
        if(value!=null) this.value = value.toString();
    }

    public CustomProperty(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }
}
