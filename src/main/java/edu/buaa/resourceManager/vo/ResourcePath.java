package edu.buaa.resourceManager.vo;


import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sjh on 17-5-20.
 * use as a simple vo
 */
public class ResourcePath implements Serializable
{
    private int id;
    private boolean online;
    private String path = "";
    private String comment = "";
    private Date metaUpdateTime;
    private List<CustomProperty> properties = new ArrayList<>();
    private List<ResourcePath> relFiles = new ArrayList<>();

    private FileEntity fileEntity = new FileEntity();

    public ResourcePath(int id, FileEntity entity, String path, boolean online, Date meta_update_t, String comment) {
        this.id = id;
        this.fileEntity = entity;
        this.path = path;
        this.online = online;
        this.metaUpdateTime = meta_update_t;
        this.comment = comment;
    }

    public ResourcePath() {

    }

    public String getComment() {
        if(null==comment){
            return "";
        }
        return comment;
    }

    public String getPath() {
        return path;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setMetaUpdateTime(Date metaUpdateTime) {
        this.metaUpdateTime = metaUpdateTime;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public List<CustomProperty> getProperties() {
        return properties;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public boolean isOnline() {
        return online;
    }

    public Date getMetaUpdateTime() {
        return metaUpdateTime==null?new Date(System.currentTimeMillis()):metaUpdateTime;
    }

    public void addRelFile(ResourcePath rf) {
        this.relFiles.add(rf);
    }

    public List<ResourcePath> getRelFiles() {
        return this.relFiles;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public void setProperties(List<CustomProperty> properties) {
        this.properties = properties;
    }

    public void setRelFiles(List<ResourcePath> relFiles) {
        this.relFiles = relFiles;
    }

    @Override
    public String toString()
    {
        return getPath();
    }


}
