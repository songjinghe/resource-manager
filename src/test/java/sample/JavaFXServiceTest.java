package sample;

/**
 * Created by sjh on 17-5-29.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class JavaFXServiceTest extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FirstLineService service = new FirstLineService();
        service.setUrl("http://baidu.com");
        service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

            @Override
            public void handle(WorkerStateEvent t) {
                System.out.println("done:" + t.getSource().getValue());
            }
        });
        service.start();
        Thread.sleep(4000);
        System.out.println(service.isRunning());
    }

    public static void main(String[] args) {
        launch();
    }

    private static class FirstLineService extends Service<String> {
        private StringProperty url = new SimpleStringProperty();

        public final void setUrl(String value) {
            url.set(value);
        }

        public final String getUrl() {
            return url.get();
        }

        public final StringProperty urlProperty() {
            return url;
        }

        protected Task<String> createTask() {
            System.out.println("task created.");
            final String _url = getUrl();
            return new Task<String>() {
                protected String call()
                        throws IOException, MalformedURLException {
                    String result = null;
                    BufferedReader in = null;
                    try {
                        URL u = new URL(_url);
                        in = new BufferedReader(
                                new InputStreamReader(u.openStream()));
                        result = in.readLine();
                    } finally {
                        if (in != null) {
                            in.close();
                        }
                    }
                    return result;
                }
            };
        }
    }
}

